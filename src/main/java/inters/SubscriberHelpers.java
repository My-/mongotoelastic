/*
 * Copyright 2015 MongoDB, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package inters;

import com.mongodb.MongoTimeoutException;
import org.bson.Document;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.UUIDs;
import org.elasticsearch.index.mapper.KeywordFieldMapper;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import static java.lang.String.format;


/**
 *  Modified Subscriber helper implementations from
 *  Source:
 *      https://github.com/mongodb/mongo-java-driver-reactivestreams/blob/master/examples/tour/src/main/tour/SubscriberHelpers.java
 *
 *  DocumentSubscriber is were all logic is.
 */
public final class SubscriberHelpers {
    private static final Logger LOG = LoggerFactory.getLogger(MongoToElastic.class);

    /**
     * A Subscriber that stores the publishers results and provides a latch so can block on completion.
     *
     * @param <T> The publishers result type
     */
    public static class ObservableSubscriber<T> implements Subscriber<T> {
        private final List<T> received;
        private final List<Throwable> errors;
        private final CountDownLatch latch;
        private volatile Subscription subscription;
        private volatile boolean completed;
        // swaggers
        List<Map<String, Object>> swaggers = new ArrayList<>();


        ObservableSubscriber() {
            this.received = new ArrayList<T>();
            this.errors = new ArrayList<Throwable>();
            this.latch = new CountDownLatch(1);
        }

        @Override
        public void onSubscribe(final Subscription s) {
            subscription = s;
        }

        @Override
        public void onNext(final T t) {
            received.add(t);
        }

        @Override
        public void onError(final Throwable t) {
            LOG.error("Mongo Reactive stream: {}", t.getMessage());
            errors.add(t);
            onComplete();
        }

        @Override
        public void onComplete() {
            completed = true;
            latch.countDown();
            LOG.info("-------------Mongo Reactive data stream finished.-----------------");
        }

        public Subscription getSubscription() {
            return subscription;
        }

        public List<T> getReceived() {
            return received;
        }

        public Throwable getError() {
            if (errors.size() > 0) {
                return errors.get(0);
            }
            return null;
        }

        public boolean isCompleted() {
            return completed;
        }

        public List<T> get(final long timeout, final TimeUnit unit) throws Throwable {
            return await(timeout, unit).getReceived();
        }

        public ObservableSubscriber<T> await() throws Throwable {
            return await(MongoToElastic.MONGO_TIMEOUT_SEC, TimeUnit.SECONDS);
        }

        public ObservableSubscriber<T> await(final long timeout, final TimeUnit unit) throws Throwable {
            subscription.request(Integer.MAX_VALUE);
            if (!latch.await(timeout, unit)) {
                throw new MongoTimeoutException("Publisher onComplete timed out");
            }
            if (!errors.isEmpty()) {
                throw errors.get(0);
            }
            return this;
        }
    }

    /**
     * A Subscriber that immediately requests Integer.MAX_VALUE onSubscribe
     *
     * @param <T> The publishers result type
     */
    public static class OperationSubscriber<T> extends ObservableSubscriber<T> {

        @Override
        public void onSubscribe(final Subscription s) {
            super.onSubscribe(s);
            s.request(Integer.MAX_VALUE);
        }
    }

    /**
     * A Subscriber that prints a message including the received items on completion
     *
     * @param <T> The publishers result type
     */
    public static class PrintSubscriber<T> extends OperationSubscriber<T> {
        private final String message;

        /**
         * A Subscriber that outputs a message onComplete.
         *
         * @param message the message to output onComplete
         */
        public PrintSubscriber(final String message) {
            this.message = message;
        }

        @Override
        public void onComplete() {
            System.out.println(format(message, getReceived()));
            super.onComplete();
        }
    }

    /**
     * A Subscriber that bundles mongo documents and then performs bulk insert to elasticsearch
     */
    public static class DocumentSubscriber extends OperationSubscriber<Document> {
        /** Elastic client **/
        private final RestHighLevelClient client;
        /** Elastic action listener **/
        private final ActionListener<BulkResponse> listener;

        // constructor
        public DocumentSubscriber(RestHighLevelClient client, ActionListener<BulkResponse> listener) {
            this.client = client;
            this.listener = listener;
        }

        /** Run on each mongo document (record) **/
        @Override
        public void onNext(final Document document) {
            super.onNext(document);

            var title = "";
            var description = "";

            // We use only subset of mongo document.
            if (document.containsKey("swagger") && ((Document)document.get("swagger")).containsKey("info") ) {
                var info = (Document) ((Document) document.get("swagger")).get("info");
                title = info.get("title", "");
                description = info.get("description", "");
            }

            var id = Optional.ofNullable(document.get("_id").toString()).orElse("");
            var apiId = document.get("apiId", "");
            var groupId = document.get("groupId", "");
            var userId = document.getString("userId");
            var isPrivate = document.get("private", Boolean.class);
            var created = document.get("created", Date.class);
            var modified = document.get("modified", Date.class);

            var lifeCycle = (Document)Optional.ofNullable(document.get("lifecycle")).orElse(new Document("published", false));
            var published = lifeCycle.get("published", Boolean.class);

            // Create map, acts like json document
            Map<String, Object> map = Map.of(
                    "id", id,
                    "apiId", apiId,
                    "groupId", groupId,
                    "userId", userId,
                    "private", isPrivate,
                    "title", title,
                    "description", description,
                    "created", created,
                    "updated", modified,
                    "published", published
            );

            // add it to bulk list...
            swaggers.add(map);

            // ... if bulk list reaches given threshold, send them to elastic and clear the list
            if( swaggers.size() == MongoToElastic.ES_BULK_INSERT_SIZE ){
                LOG.info("Adding {} documents to Elasticsearch.", swaggers.size());
                var request = new BulkRequest();
                swaggers.forEach(swagger ->
                        request.add(new IndexRequest(MongoToElastic.INDEX).source(swagger))
                );

                client.bulkAsync(request, RequestOptions.DEFAULT, listener);
                swaggers.clear();
            }
        }

        /** Run on reactive stream completion **/
        @Override
        public void onComplete() {
            // On completion dump remaining bulk list to elasticsearch
            var request = new BulkRequest();
            swaggers.forEach(swagger ->
                    request.add(new IndexRequest(MongoToElastic.INDEX).source(swagger))
            );

            client.bulkAsync(request, RequestOptions.DEFAULT, listener);
            swaggers.clear();

//            try {
//                client.close();
//            } catch (IOException e) { e.printStackTrace(); }

            super.onComplete();
        }
    }

    private SubscriberHelpers() {
    }

}