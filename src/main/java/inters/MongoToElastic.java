package inters;


import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import org.apache.http.HttpHost;
import org.bson.Document;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MongoToElastic {

    public static final String INDEX = "swaggers";
    public static final String ES_HOSTNANE = "localhost";
    public static final int ES_PORT = 9200;

    public static final int MONGO_TIMEOUT_SEC = 60;
    public static final int ES_BULK_INSERT_SIZE = 1000;

    private static final Logger LOG = LoggerFactory.getLogger(MongoToElastic.class);

    // Runner
    public static void main(String[] args) throws Throwable {
        LOG.info("Starting main()");

        // Elastic
        var client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(ES_HOSTNANE, ES_PORT, "http")));

        var request = new CreateIndexRequest(MongoToElastic.INDEX);
        request.settings("" +
                        "{" +
                        "\"analysis\": {\n" +
                        "     \"analyzer\": {\n" +
                        "       \"my_english_analyzer\": {\n" +
                        "         \"type\": \"standard\",\n" +
                        "         \"max_token_length\": 5,\n" +
                        "         \"stopwords\": \"english\"\n" +
                        "       }\n" +
                        "     }\n" +
                        "   }" +
                        "}",
                XContentType.JSON
        );
        request.mapping("" +
                        "\"properties\": {\n" +
                        "           \"groupId\": {\n" +
                        "               \"type\": \"keyword\"\n" +
                        "           },\n" +
                        "           \"apiId\": {\n" +
                        "               \"type\": \"keyword\"\n" +
                        "           },\n" +
                        "           \"title\": {\n" +
                        "               \"type\": \"keyword\"\n" +
                        "           },\n" +
                        "           \"description\": {\n" +
                        "               \"type\": \"text\",\n" +
                        "               \"analyzer\": \"my_english_analyzer\"\n" +
                        "           }\n" +
                        "       }",
                XContentType.JSON
        );



        try{
            // Trows is index already exist
            var createIndexResponce = client.indices().create(request, RequestOptions.DEFAULT);
        }catch (ElasticsearchException e){ e.guessRootCauses(); }

        var mongoClient = MongoClients.create();
        var registryDB = mongoClient.getDatabase("registry");

        MongoCollection<Document> collection = registryDB.getCollection("swaggers");

        Publisher<Document> publisher =  collection.find();

        // This listener is called each time elasticsearch bulk insert done (successfully of with error)
        var listener = new ActionListener<BulkResponse>(){
            @Override
            public void onResponse(BulkResponse bulkItemResponses) {
                LOG.info("Successfully added documents to Elasticsearch");
            }

            @Override
            public void onFailure(Exception e) {
                LOG.error("Add documents to Elastic: {}", e.getMessage());
            }
        };

        SubscriberHelpers.DocumentSubscriber subscriber = new SubscriberHelpers.DocumentSubscriber(client,  listener);
        publisher.subscribe(subscriber);

        subscriber.await();

//        client.close();
        LOG.info("Done with main()");
    }
}

