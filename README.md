# Mongo to Elasticsearch Dumper

##### Git repo: [https://gitlab.com/My-/mongotoelastic](https://gitlab.com/My-/mongotoelastic)

To pull documents from MongoDb program uses [Reactive Mongo driver](http://mongodb.github.io/mongo-java-driver-reactivestreams/)
and it dumps to [Elasticsearch asynchronously](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.3/java-rest-high-document-bulk.html#java-rest-high-document-bulk-async).

#### Some Reactivestream advantages
- Back pressure - "ask for data when you need it"
- Errors have own channel - Exceptions are send to error channel were they can be dealt with (like JS Promises).

Source: Venkat Subramaniam video ["Java Streams vs Reactive Streams"](https://www.youtube.com/watch?v=kG2SEcl1aMM&t=1678s)

#### How "Dumper" works
1. Connects to MongoDb and Elasticsearch
2. Reactively pulls documents from Mongo (by asking `subscription.request(amount)`)
3. On each document `Subscriber<T>` interface method `onNext(T)` is called.
4. This method prepares document (creates `Map` as JSON representation) and adds this prepared document to the `List<Map>`
5. When `List<Map>` reaches the desired elastic bulk insert size, then a whole list is dumped in bulk to elastic using asynchronous execution.
6. `List<Map>` is cleared and process repeats ( from step 3) until all Mongo data is retrieved.
7. Then `Subscriber<T>` method `onComplete()` is called.
8. At this point the remaining data in the list is dumped to elastic.
